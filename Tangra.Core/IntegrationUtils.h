/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#pragma once

#include "cross_platform.h"

/* Make sure functions are exported with C linkage under C++ compilers. */
#ifdef __cplusplus
extern "C"
{
#endif

DLL_PUBLIC void IntergationManagerStartNew(long width, long height, bool isMedianAveraging);
DLL_PUBLIC void IntegrationManagerAddFrame(unsigned long* framePixels);
DLL_PUBLIC void IntegrationManagerAddFrameEx(unsigned long* framePixels, bool isLittleEndian, long bpp);
DLL_PUBLIC void IntegrationManagerProduceIntegratedFrame(unsigned long* framePixels);
DLL_PUBLIC void IntegrationManagerFreeResources();
DLL_PUBLIC long IntegrationManagerGetFirstFrameToIntegrate(long producedFirstFrame, long frameCount, bool isSlidingIntegration);

#ifdef __cplusplus
} // __cplusplus defined.
#endif