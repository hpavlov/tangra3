﻿namespace Tangra.VideoOperations.Astrometry
{
	partial class frmEditConfigName
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEditConfigName));
			this.btnCancel = new System.Windows.Forms.Button();
			this.btnOK = new System.Windows.Forms.Button();
			this.tbxConfigName = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.tbxSolvedCellX = new System.Windows.Forms.TextBox();
			this.tbxSolvedFocalLength = new System.Windows.Forms.TextBox();
			this.tbxSolvedCellY = new System.Windows.Forms.TextBox();
			this.label18 = new System.Windows.Forms.Label();
			this.label19 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.pnlSolvedPlateConf = new System.Windows.Forms.Panel();
			this.cbxEditConfig = new System.Windows.Forms.CheckBox();
			this.pnlSolvedPlateConf.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Location = new System.Drawing.Point(191, 251);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 36;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			// 
			// btnOK
			// 
			this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnOK.Location = new System.Drawing.Point(110, 251);
			this.btnOK.Name = "btnOK";
			this.btnOK.Size = new System.Drawing.Size(75, 23);
			this.btnOK.TabIndex = 35;
			this.btnOK.Text = "OK";
			this.btnOK.UseVisualStyleBackColor = true;
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// tbxConfigName
			// 
			this.tbxConfigName.Location = new System.Drawing.Point(16, 126);
			this.tbxConfigName.Name = "tbxConfigName";
			this.tbxConfigName.Size = new System.Drawing.Size(353, 20);
			this.tbxConfigName.TabIndex = 34;
			this.tbxConfigName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbxConfigName_KeyPress);
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(13, 110);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(103, 13);
			this.label11.TabIndex = 33;
			this.label11.Text = "Configuration Name:";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 15);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(365, 82);
			this.label1.TabIndex = 37;
			this.label1.Text = resources.GetString("label1.Text");
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(137, 30);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(15, 13);
			this.label17.TabIndex = 73;
			this.label17.Text = "m";
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(137, 4);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(15, 13);
			this.label15.TabIndex = 71;
			this.label15.Text = "m";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(3, 4);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(60, 13);
			this.label12.TabIndex = 67;
			this.label12.Text = "Pixel Width";
			// 
			// tbxSolvedCellX
			// 
			this.tbxSolvedCellX.Location = new System.Drawing.Point(68, 1);
			this.tbxSolvedCellX.Name = "tbxSolvedCellX";
			this.tbxSolvedCellX.Size = new System.Drawing.Size(54, 20);
			this.tbxSolvedCellX.TabIndex = 66;
			// 
			// tbxSolvedFocalLength
			// 
			this.tbxSolvedFocalLength.Location = new System.Drawing.Point(255, 1);
			this.tbxSolvedFocalLength.Name = "tbxSolvedFocalLength";
			this.tbxSolvedFocalLength.Size = new System.Drawing.Size(54, 20);
			this.tbxSolvedFocalLength.TabIndex = 74;
			// 
			// tbxSolvedCellY
			// 
			this.tbxSolvedCellY.Location = new System.Drawing.Point(68, 27);
			this.tbxSolvedCellY.Name = "tbxSolvedCellY";
			this.tbxSolvedCellY.Size = new System.Drawing.Size(54, 20);
			this.tbxSolvedCellY.TabIndex = 68;
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Location = new System.Drawing.Point(183, 4);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(69, 13);
			this.label18.TabIndex = 75;
			this.label18.Text = "Focal Length";
			// 
			// label19
			// 
			this.label19.AutoSize = true;
			this.label19.Location = new System.Drawing.Point(315, 4);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(23, 13);
			this.label19.TabIndex = 76;
			this.label19.Text = "mm";
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(3, 30);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(63, 13);
			this.label13.TabIndex = 69;
			this.label13.Text = "Pixel Height";
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Font = new System.Drawing.Font("Symbol", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
			this.label14.Location = new System.Drawing.Point(128, 4);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(13, 13);
			this.label14.TabIndex = 70;
			this.label14.Text = "m";
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Font = new System.Drawing.Font("Symbol", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
			this.label16.Location = new System.Drawing.Point(128, 30);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(13, 13);
			this.label16.TabIndex = 72;
			this.label16.Text = "m";
			// 
			// pnlSolvedPlateConf
			// 
			this.pnlSolvedPlateConf.Controls.Add(this.label12);
			this.pnlSolvedPlateConf.Controls.Add(this.label17);
			this.pnlSolvedPlateConf.Controls.Add(this.label16);
			this.pnlSolvedPlateConf.Controls.Add(this.label15);
			this.pnlSolvedPlateConf.Controls.Add(this.label14);
			this.pnlSolvedPlateConf.Controls.Add(this.label13);
			this.pnlSolvedPlateConf.Controls.Add(this.tbxSolvedCellX);
			this.pnlSolvedPlateConf.Controls.Add(this.label19);
			this.pnlSolvedPlateConf.Controls.Add(this.tbxSolvedFocalLength);
			this.pnlSolvedPlateConf.Controls.Add(this.label18);
			this.pnlSolvedPlateConf.Controls.Add(this.tbxSolvedCellY);
			this.pnlSolvedPlateConf.Enabled = false;
			this.pnlSolvedPlateConf.Location = new System.Drawing.Point(12, 179);
			this.pnlSolvedPlateConf.Name = "pnlSolvedPlateConf";
			this.pnlSolvedPlateConf.Size = new System.Drawing.Size(354, 54);
			this.pnlSolvedPlateConf.TabIndex = 77;
			// 
			// cbxEditConfig
			// 
			this.cbxEditConfig.AutoSize = true;
			this.cbxEditConfig.Location = new System.Drawing.Point(16, 155);
			this.cbxEditConfig.Name = "cbxEditConfig";
			this.cbxEditConfig.Size = new System.Drawing.Size(166, 17);
			this.cbxEditConfig.TabIndex = 78;
			this.cbxEditConfig.Text = "Edit Effective Plate Constants";
			this.cbxEditConfig.UseVisualStyleBackColor = true;
			this.cbxEditConfig.CheckedChanged += new System.EventHandler(this.cbxEditConfig_CheckedChanged);
			// 
			// frmEditConfigName
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(383, 286);
			this.Controls.Add(this.cbxEditConfig);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOK);
			this.Controls.Add(this.tbxConfigName);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.pnlSolvedPlateConf);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmEditConfigName";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Scope + Focal Reducer + Recorder + Grabber";
			this.pnlSolvedPlateConf.ResumeLayout(false);
			this.pnlSolvedPlateConf.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.TextBox tbxConfigName;
        private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox tbxSolvedCellX;
		private System.Windows.Forms.TextBox tbxSolvedFocalLength;
		private System.Windows.Forms.TextBox tbxSolvedCellY;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Panel pnlSolvedPlateConf;
		private System.Windows.Forms.CheckBox cbxEditConfig;
	}
}